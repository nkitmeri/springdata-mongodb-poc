package net.amdtelecom.springdata.mongodb.poc.repository;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Configuration
@EnableMongoRepositories(basePackageClasses = TestConfiguration.class)
public class TestConfiguration extends AbstractMongoConfiguration {

    private static final String dateFormat = "yyyy-MM-dd'T'hh:mm:ssXXX";

    @Override
    protected String getDatabaseName() {
        return "springdata-poc-test";
    }

    @Override
    @Bean
    public Mongo mongo() throws Exception {
        return new MongoClient("127.0.0.1");
    }

    @Bean
    public DateFormat dateFormat() {
        return new SimpleDateFormat(dateFormat);
    }

}
