package net.amdtelecom.springdata.mongodb.poc.repository;

import com.lordofthejars.nosqlunit.annotation.ShouldMatchDataSet;
import com.lordofthejars.nosqlunit.annotation.UsingDataSet;
import com.lordofthejars.nosqlunit.mongodb.MongoDbRule;
import net.amdtelecom.springdata.mongodb.poc.model.Account;
import net.amdtelecom.springdata.mongodb.poc.model.User;
import org.apache.commons.lang3.StringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.lordofthejars.nosqlunit.mongodb.MongoDbRule.MongoDbRuleBuilder.newMongoDbRule;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class UserRepositoryTester {

    @Rule
    public MongoDbRule mongoDbRule = newMongoDbRule().defaultSpringMongoDb("springdata-poc-test");

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private UserRepository userRepository;

    // -----------------------------------------
    //  Tests: unnecessary tests
    //         just for demonstration
    //         of some of theCrudRepository &
    //         PagingAndSortingRepository
    //         methods
    // -----------------------------------------

    // ---------------------------------
    // save
    // ---------------------------------

    @Test
    @UsingDataSet(locations = {"Initial-UserDataSet.json"})
    @ShouldMatchDataSet(location = "Expected-UserDataSet.json")
    public void test_saveUser() {

        Account account = new Account();
        account.setId("i1");

        User user = new User();
        user.setId("u3");
        user.setFirstName("John");
        user.setLastName("Smith");
        user.setUserName("john123");
        user.setEmail("john123@example2.com");
        user.setOwnerRef(account);

        userRepository.save(user);
    }

    @Test
    @UsingDataSet(locations = {"EmptyUserDataSet.json"})
    @ShouldMatchDataSet(location = "Expected-UserDataSet.json")
    public void test_saveUsers() {

        Account account1 = new Account();
        account1.setId("c1");

        Account account2 = new Account();
        account2.setId("i1");

        User user1 = new User();
        user1.setId("u1");
        user1.setFirstName("Allan");
        user1.setLastName("Connolly");
        user1.setUserName("allan123");
        user1.setEmail("allan123@example1.com");
        user1.setOwnerRef(account1);

        User user2 = new User();
        user2.setId("u2");
        user2.setFirstName("Randy");
        user2.setLastName("Carver");
        user2.setUserName("randy123");
        user2.setEmail("randy123@example1.com");
        user2.setOwnerRef(account1);

        User user3 = new User();
        user3.setId("u3");
        user3.setFirstName("John");
        user3.setLastName("Smith");
        user3.setUserName("john123");
        user3.setEmail("john123@example2.com");
        user3.setOwnerRef(account2);

        userRepository.save(Arrays.asList(user1, user2, user3));
    }

    // ---------------------------------
    // delete
    // ---------------------------------

    @Test
    @UsingDataSet(locations = {"Expected-UserDataSet.json"})
    @ShouldMatchDataSet(location = "Initial-UserDataSet.json")
    public void test_deleteUserById() {

        userRepository.delete("u3");
    }

    @Test
    @UsingDataSet(locations = {"Expected-UserDataSet.json"})
    @ShouldMatchDataSet(location = "Initial-UserDataSet.json")
    public void test_deleteUserByObj() {

        User user = new User();
        user.setId("u3");

        userRepository.delete(user);
    }

    @Test
    @UsingDataSet(locations = {"Initial-UserDataSet.json"})
    @ShouldMatchDataSet(location = "EmptyUserDataSet.json")
    public void test_deleteAllUsers() {

        userRepository.deleteAll();
    }

    // ---------------------------------
    // count
    // ---------------------------------

    @Test
    @UsingDataSet(locations = {"Initial-UserDataSet.json"})
    public void test_countAllUsers() {

        assertEquals(2, userRepository.count());
    }

    // ---------------------------------
    // find
    // ---------------------------------

    @Test
    @UsingDataSet(locations = {"Expected-UserDataSet.json"})
    public void test_fetchUsersInPages() {

        // Single element pages staring from the first page
        Pageable pageable = new PageRequest(0, 1);

        Page<User> page = userRepository.findAll(pageable);

        assertEquals(3, page.getTotalPages());
        assertEquals(3, page.getTotalElements());

        Iterator<User> iterator = page.iterator();

        // Ensure 1st page has the 1st user
        assertEquals("u1", iterator.next().getId());
        // Ensure page has only one element, as it been asked
        assertFalse(iterator.hasNext());

        page = userRepository.findAll(page.nextPageable());
        iterator = page.iterator();

        // Ensure 2nd page has the 2nd user
        assertEquals("u2", iterator.next().getId());
        // Ensure page has only one element, as it been asked
        assertFalse(iterator.hasNext());

        page = userRepository.findAll(page.nextPageable());
        iterator = page.iterator();

        // Ensure 3rd page has the 3rd user
        assertEquals("u3", iterator.next().getId());
        // Ensure page has only one element, as it been asked
        assertFalse(iterator.hasNext());

        // Ensure that we have fetched all pages
        assertFalse(page.hasNext());
    }

    @Test
    @UsingDataSet(locations = {"Initial-UserDataSet.json"})
    public void test_findUserById() {

        User user = userRepository.findOne("u1");

        assertEquals("u1", user.getId());
        assertEquals("Allan", user.getFirstName());
    }

    @Test
    @UsingDataSet(locations = {"Initial-UserDataSet.json"})
    public void test_findUsersByIds() {

        List<String> searchIds = Arrays.asList("u1", "u2", "u12345");

        Iterable<User> users = userRepository.findAll(searchIds);

        List<User> usersList = StreamSupport
                .stream(users.spliterator(), false)
                .collect(Collectors.toList());

        assertEquals(2, usersList.size());
        assertEquals("u1", usersList.get(0).getId());
        assertEquals("u2", usersList.get(1).getId());
    }


    // ==============================================================

    // -----------------------------------------
    //
    //  Tests: Repository Interface Methods
    //
    // -----------------------------------------

    // ----------------------------------
    // findByOwnerRef_IdAndUserName
    // ----------------------------------

    @Test
    @UsingDataSet(locations = {"Initial-ExtendedUserDataSet.json"})
    public void test_findByOwnerRef_IdAndUserName_whenUserNameExists_returnsCorrectUser() {

        String ownerRefId = "c1";
        String userName = "joe123";

        String id = "u4";
        String firstName = "Joe";
        String lastName = "Doe";
        String email = "joe123@other.com";

        User user = userRepository.findByOwnerRef_IdAndUserName(ownerRefId, userName);

        assertEquals(id, user.getId());
        assertEquals(userName, user.getUserName());
        assertEquals(firstName, user.getFirstName());
        assertEquals(lastName, user.getLastName());
        assertEquals(email, user.getEmail());
        assertEquals(ownerRefId, user.getOwnerRef().getId());
    }

    @Test
    @UsingDataSet(locations = {"Initial-ExtendedUserDataSetSameUserName.json"})
    public void test_findByOwnerRef_IdAndUserName_whenUserNameExistsMoreThanOnce_returnsCorrectUser() {

        String ownerRefId = "c1";
        String userName = "joe123";

        String id = "u1";
        String firstName = "Allan";
        String lastName = "Connolly";
        String email = "allan123@example1.com";

        User user = userRepository.findByOwnerRef_IdAndUserName(ownerRefId, userName);

        assertEquals(id, user.getId());
        assertEquals(userName, user.getUserName());
        assertEquals(firstName, user.getFirstName());
        assertEquals(lastName, user.getLastName());
        assertEquals(email, user.getEmail());
        assertEquals(ownerRefId, user.getOwnerRef().getId());
    }

    // ----------------------------------
    // deleteByOwnerRef_IdAndUserName
    // ----------------------------------

    @Test
    @UsingDataSet(locations = {"Initial-ExtendedUserDataSetSameUserName.json"})
    @ShouldMatchDataSet(location = "Expected-ExtendedUserDataSetSameUserName.json")
    public void test_deleteByOwnerRef_IdAndUserName_whenUserNameExistsDeletesUser() {

        String ownerRefId = "c1";
        String userName = "joe123";

        long deleted = userRepository.deleteByOwnerRef_IdAndUserName(ownerRefId, userName);

        assertEquals(2, deleted);
    }

    // ----------------------------------
    // findByOwnerRef_IdAndEmailProvider
    // ----------------------------------

    @Test
    @UsingDataSet(locations = {"Initial-ExtendedUserDataSet.json"})
    public void test_findByOwnerRef_IdAndEmailProvider_returnsCorrectUsers() {

        String ownerRefId = "c1";
        String emailProvider = "EXAMPLE1";

        Collection<User> users = userRepository.findByOwnerRefIdAndEmailProvider(ownerRefId, emailProvider);

        users.forEach(user -> {
            assertTrue(StringUtils.containsIgnoreCase(user.getEmail(), emailProvider));
            assertEquals(ownerRefId, user.getOwnerRef().getId());
        });

        assertEquals(2, users.size());
    }

    // ----------------------------------
    // countByOwnerRef_IdAndFirstNameIn
    // ----------------------------------

    @Test
    @UsingDataSet(locations = {"Initial-ExtendedUserDataSet.json"})
    public void test_CountByOwnerRef_IdAndFirstNameIn_whenFirstNameExists_returnsCorrectOccurs() {

        String ownerRefId = "c1";
        List<String> firstNames = Arrays.asList("Allan", "Randy", "NonExisting");

        long occurrences = userRepository.countByOwnerRef_IdAndFirstNameIn(ownerRefId, firstNames);

        assertEquals(2, occurrences);
    }

    // ----------------------------------
    // countByOwnerRef_IdAndLastName
    // ----------------------------------

    @Test
    @UsingDataSet(locations = {"Initial-ExtendedUserDataSetSameLastName.json"})
    public void test_CountByOwnerRef_IdAndFirstNameIn_whenLastNameExists_returnsCorrectOccurs() {

        String ownerRefId = "c1";
        String lastName = "Connolly";

        long occurrences = userRepository.countByOwnerRef_IdAndLastName(ownerRefId, lastName);

        assertEquals(2, occurrences);
    }

    // ----------------------------------
    // findByOwnerRef_IdOrderByLastName
    // ----------------------------------

    @Test
    @UsingDataSet(locations = {"Initial-ExtendedUserDataSet.json"})
    public void test_findByOwnerRef_IdOrderByLastName_whenAccountHasUsers_returnsWithCorrectOrder() {

        String ownerRefId = "c1";

        Collection<User> users = userRepository.findByOwnerRef_IdOrderByLastName(ownerRefId);
        List<User> usersList = new ArrayList<>(users);

        assertEquals(3, users.size());
        assertEquals("Carver", usersList.get(0).getLastName());
        assertEquals("Connolly", usersList.get(1).getLastName());
        assertEquals("Doe", usersList.get(2).getLastName());
    }

    // =============================================================

    // -----------------------------------------
    //
    //  Tests: Custom repository methods
    //
    // -----------------------------------------

    // ---------------------------------------
    // groupAndCountUsersOfAccountByFirstName
    // ---------------------------------------

    @Test
    @UsingDataSet(locations = "Initial-ExtendedUserDataSetSameFirstName.json")
    public void test_groupAndCountUsersOfAccountByFirstName_whenUsersExists_returnsCorrectGrouping() {

        Account account = new Account();
        account.setId("c1");

        Map<String, Long> expectedMap = new HashMap<String, Long>() {{
            put("Allan", 2L);
            put("Randy", 1L);
        }};

        Map<String, Long> firstNamesMap = userRepository.groupAndCountUsersOfAccountByFirstName(account);

        assertEquals(2, firstNamesMap.size());
        assertEquals(expectedMap, firstNamesMap);
    }
}
