package net.amdtelecom.springdata.mongodb.poc.repository;

import com.lordofthejars.nosqlunit.annotation.CustomComparisonStrategy;
import com.lordofthejars.nosqlunit.annotation.IgnorePropertyValue;
import com.lordofthejars.nosqlunit.annotation.ShouldMatchDataSet;
import com.lordofthejars.nosqlunit.annotation.UsingDataSet;
import com.lordofthejars.nosqlunit.mongodb.MongoDbRule;
import com.lordofthejars.nosqlunit.mongodb.MongoFlexibleComparisonStrategy;
import net.amdtelecom.springdata.mongodb.poc.model.Account;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.DateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;

import static com.lordofthejars.nosqlunit.mongodb.MongoDbRule.MongoDbRuleBuilder.newMongoDbRule;
import static net.amdtelecom.springdata.mongodb.poc.model.Account.OrganizationType;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
@CustomComparisonStrategy(comparisonStrategy = MongoFlexibleComparisonStrategy.class)
public class AccountRepositoryTester {

    @Rule
    public MongoDbRule mongoDbRule = newMongoDbRule().defaultSpringMongoDb("springdata-poc-test");

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private DateFormat dateFormat;



    // -----------------------------------------
    //
    //  Tests: Demo of the importance of compound
    //         index fields order
    //
    // -----------------------------------------

    @Test
    public void test_compareTimesInQueriesBetweenIndexedAndNotIndexedFields() {

        accountRepository.deleteAll();

        List<Account> testAccounts = new ArrayList<>();

        for (int i = 0; i < 100000; i++) {
            Account account = new Account(
                    "id_" + i,
                    "organizationName_" + i,
                    "industry",
                    (i % 2 == 0) ? OrganizationType.COMPANY : OrganizationType.INDIVIDUAL,
                    new HashSet<>(),
                    new Date(System.currentTimeMillis())
            );

            testAccounts.add(account);
        }

        long start;
        long end;

        accountRepository.save(testAccounts);

        // Fields of compound index in right order
        start = System.currentTimeMillis();
        accountRepository.findAll(new Sort(Sort.Direction.ASC, "industry", "type"));
        end = System.currentTimeMillis();
        System.out.println("Indexed: " + (end - start));

        // Fields of compound index in wrong order
        start = System.currentTimeMillis();
        accountRepository.findAll(new Sort(Sort.Direction.ASC, "type", "industry"));
        end = System.currentTimeMillis();
        System.out.println("NotIndexed: " + (end - start));

        accountRepository.deleteAll();
    }

    // =============================================================

    // -----------------------------------------
    //
    //  Tests: Repository Interface Methods
    //
    // -----------------------------------------

    // ----------------------------------
    // findByOrganizationName
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_findByOrganizationName_whenNameExists_returnsCorrectAccount() {

        Account account = accountRepository.findByOrganizationName("SuperCompany1");

        assertEquals("c1", account.getId());
        assertEquals("SuperCompany1", account.getOrganizationName());
        assertEquals("Financial", account.getIndustry());
        assertEquals(OrganizationType.COMPANY, account.getType());
        assertEquals(OffsetDateTime.parse("2014-01-01T10:00:00Z"),
                OffsetDateTime.ofInstant(account.getCreatedAt().toInstant(), ZoneId.ofOffset("", ZoneOffset.UTC)));
    }

    // ----------------------------------
    // findByTypeInOrderByCreatedAtDesc
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_findByTypeInOrderByCreatedAtDesc_withOneType_returnsLastCreatedAccountForType() {

        Account account = accountRepository.findByTypeInOrderByCreatedAtDesc(
                Arrays.asList(OrganizationType.COMPANY));

        assertEquals("c3", account.getId());
        assertEquals("SuperCompany3", account.getOrganizationName());
        assertEquals("Online Services", account.getIndustry());
        assertEquals(OrganizationType.COMPANY, account.getType());
        assertEquals(5, account.getNoOfUsers());
        assertEquals(OffsetDateTime.parse("2016-03-01T17:00:00Z"),
                OffsetDateTime.ofInstant(account.getCreatedAt().toInstant(), ZoneId.ofOffset("", ZoneOffset.UTC)));
    }

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_findByTypeInOrderByCreatedAtDesc_withAllTypes_returnsLastCreatedAccount() {

        Account account = accountRepository.findByTypeInOrderByCreatedAtDesc(
                Arrays.asList(OrganizationType.COMPANY, OrganizationType.INDIVIDUAL));

        assertEquals("i1", account.getId());
        assertEquals("SuperIndividual1", account.getOrganizationName());
        assertEquals("Online Services", account.getIndustry());
        assertEquals(OrganizationType.INDIVIDUAL, account.getType());
        assertEquals(1, account.getNoOfUsers());
        assertEquals(OffsetDateTime.parse("2016-05-05T10:00:00Z"),
                OffsetDateTime.ofInstant(account.getCreatedAt().toInstant(), ZoneId.ofOffset("", ZoneOffset.UTC)));
    }

    // ----------------------------------
    // findByCreatedAtLessThanEqual
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_findByCreatedAtLessThanEqual_whenAccountsExists_returnsAllTheAccountsCreatedBeforeInclusive() throws Exception {

        Date date = dateFormat.parse("2016-03-01T17:00:00Z");

        Collection<Account> accounts = accountRepository.findByCreatedAtLessThanEqual(date);

        accounts.forEach(account -> assertTrue(date.compareTo(account.getCreatedAt()) >= 0));

        assertEquals(3, accounts.size());
    }

    // ----------------------------------
    // findByCreatedAtBefore
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_findByCreatedAtBefore_whenAccountsExists_returnsAllTheAccountsCreatedBeforeExclusive() throws Exception {

        Date date = dateFormat.parse("2016-03-01T17:00:00Z");

        Collection<Account> accounts = accountRepository.findByCreatedAtBefore(date);

        accounts.forEach(account -> assertTrue(date.compareTo(account.getCreatedAt()) > 0));

        assertEquals(2, accounts.size());
    }

    // ----------------------------------
    // findByCreatedAtGreaterThanEqual
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_findByCreatedAtGreaterThanEqual_whenAccountsExists_returnsAllTheAccountsCreatedAfterInclusive() throws Exception {

        Date date = dateFormat.parse("2015-06-06T12:00:00Z");

        Collection<Account> accounts = accountRepository.findByCreatedAtGreaterThanEqual(date);

        accounts.forEach(account -> assertTrue(date.compareTo(account.getCreatedAt()) <= 0));

        assertEquals(3, accounts.size());
    }

    // ----------------------------------
    // findByCreatedAtAfter
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_findByCreatedAtAfter_whenAccountsExists_returnsAllTheAccountsCreatedAfterExclusive() throws Exception {

        Date date = dateFormat.parse("2015-06-06T00:00:00Z");

        Collection<Account> accounts = accountRepository.findByCreatedAtAfter(date);

        accounts.forEach(account -> assertTrue(date.compareTo(account.getCreatedAt()) < 0));

        assertEquals(2, accounts.size());
    }

    // ----------------------------------
    // findByCreatedAtBetweenInclusive
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_findByCreatedAtBetweenInclusive_whenAccountsExists_returnsAllTheAccountsCreatedBetweenInclusive() throws Exception {

        Date startingDate = dateFormat.parse("2015-06-06T00:00:00Z");
        Date endingDate = dateFormat.parse("2016-03-01T17:00:00Z");

        Collection<Account> accounts = accountRepository.findByCreatedAtBetweenInclusive(startingDate, endingDate);

        accounts.forEach(account -> assertTrue(startingDate.compareTo(account.getCreatedAt()) <= 0 &&
                endingDate.compareTo(account.getCreatedAt()) >= 0));

        assertEquals(2, accounts.size());
    }

    // ----------------------------------
    // findByCreatedAtBetween
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_findByCreatedAtBetween_whenAccountsExists_returnsAllTheAccountsCreatedBetweenExclusive() throws Exception {

        Date startingDate = dateFormat.parse("2014-01-01T10:00:00Z");
        Date endingDate = dateFormat.parse("2016-05-05T10:00:00Z");

        Collection<Account> accounts = accountRepository.findByCreatedAtBetween(startingDate, endingDate);

        accounts.forEach(account -> assertTrue(startingDate.compareTo(account.getCreatedAt()) < 0 &&
                endingDate.compareTo(account.getCreatedAt()) > 0));

        assertEquals(2, accounts.size());
    }

    // ----------------------------------
    // findByIndustryIn
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_findByIndustryIn_whenAccountsExists_returnsCorrectAccounts() throws Exception {

        List<String> industries = Arrays.asList("Financial", "Online Services", "NonExisting");

        Collection<Account> accounts = accountRepository.findByIndustryIn(industries);

        accounts.forEach(account -> assertTrue("Financial".equals(account.getIndustry())
                || "Online Services".equals(account.getIndustry())));

        assertEquals(3, accounts.size());
    }

    // ----------------------------------
    // findByIndustryNotIn
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_findByIndustryNotIn_whenAccountsExists_returnsCorrectAccounts() throws Exception {

        List<String> industries = Arrays.asList("Financial", "Online Services", "NonExisting");

        Collection<Account> accounts = accountRepository.findByIndustryNotIn(industries);

        accounts.forEach(account -> assertTrue("Airlines".equals(account.getIndustry())));

        assertEquals(1, accounts.size());
    }

    // ----------------------------------
    // countByNoOfUsersExists
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_countByNoOfUsersExists_whenActiveAccountsExists_returnsCorrectNumber() throws Exception {

        assertEquals(3, accountRepository.countByNoOfUsersExists(true));
    }

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    public void test_countByNoOfUsersExists_whenInActiveAccountsExists_returnsCorrectNumber() throws Exception {

        assertEquals(1, accountRepository.countByNoOfUsersExists(false));
    }

    // =============================================================

    // -----------------------------------------
    //
    //  Tests: Custom repository methods
    //
    // -----------------------------------------

    // ----------------------------------
    // updateAccountsField
    // ----------------------------------

    @Test
    @UsingDataSet(locations = "Initial-AccountDataSet.json")
    @ShouldMatchDataSet(location = "Expected-AccountDataSet.json")
    @IgnorePropertyValue(properties = {"account.updatedAt"}) // Use class annotation @CustomComparisonStrategy to use this
    public void test_UpdateAccountsField_whenAccountExistsAndFieldExists_updatesAccount() {

        long updated = accountRepository.updateAccountsField("c1", "industry", "Financial & Online Services");

        assertEquals(1, updated);
    }
}
