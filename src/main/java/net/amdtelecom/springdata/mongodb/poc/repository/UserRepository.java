package net.amdtelecom.springdata.mongodb.poc.repository;

import net.amdtelecom.springdata.mongodb.poc.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Collection;

public interface UserRepository extends PagingAndSortingRepository<User, String>, UserRepositoryCustom {


    // -------- userName ------------

    User findByOwnerRef_IdAndUserName(String ownerRefId, String userName);

    long deleteByOwnerRef_IdAndUserName(String ownerRefId, String userName);


    // -------- email -------------

    @Query(value="{ 'ownerRef.$id' : ?0, 'email': {$regex: '?1', $options: 'i'} }", fields="{ 'ownerRef': 1, 'email': 1}")
    Collection<User> findByOwnerRefIdAndEmailProvider(String ownerRefId, String provider);

    // -------- firstName ---------

    long countByOwnerRef_IdAndFirstNameIn(String ownerRefId, Collection<String> firstNames);


    // -------- lastName ---------

    long countByOwnerRef_IdAndLastName(String ownerRefId, String lastName);


    // -------- ownerRef (id) -------------

    Collection<User> findByOwnerRef_IdOrderByLastName(String ownerRefId);

}
