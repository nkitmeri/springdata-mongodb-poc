package net.amdtelecom.springdata.mongodb.poc.repository;

import net.amdtelecom.springdata.mongodb.poc.model.Account;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Collection;
import java.util.Date;

import static net.amdtelecom.springdata.mongodb.poc.model.Account.OrganizationType;

public interface AccountRepository extends PagingAndSortingRepository<Account, String>, AccountRepositoryCustom {

    Account findByOrganizationName(String organizationName);

    // find the last created account (for a single or any types)
    Account findByTypeInOrderByCreatedAtDesc(Collection<OrganizationType> types);

    // find by created at before given date (inclusive)
    Collection<Account> findByCreatedAtLessThanEqual(Date date);

    // find by created at before given date (exclusive)
    Collection<Account> findByCreatedAtBefore(Date date);

    // find by created at after given date (inclusive)
    Collection<Account> findByCreatedAtGreaterThanEqual(Date date);

    // find by created at after given date (exclusive)
    Collection<Account> findByCreatedAtAfter(Date date);

    // find by created at between date range (inclusive)
    @Query(value = "{'createdAt': {$gte: ?0, $lte: ?1}}")
    Collection<Account> findByCreatedAtBetweenInclusive(Date startingDate, Date endingDate);

    // find by created at between date range (exclusive)
    Collection<Account> findByCreatedAtBetween(Date startingDate, Date endingDate);

    Collection<Account> findByIndustryIn(Collection<String> industries);

    Collection<Account> findByIndustryNotIn(Collection<String> industries);

    // count active or inactive accounts (i.e having users or not)
    long countByNoOfUsersExists(boolean exists);
}
