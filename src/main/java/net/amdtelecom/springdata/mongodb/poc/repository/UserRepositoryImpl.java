package net.amdtelecom.springdata.mongodb.poc.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import net.amdtelecom.springdata.mongodb.poc.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.query.Criteria.where;

public class UserRepositoryImpl implements UserRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    /*
    db.user.aggregate([
        {$match: {"ownerRef": {"$ref" : "account", "$id" : "c1"}}},
        {$group: {"_id": null, "names": {$push: "$firstName"}}},
        {$unwind: "$names"},
        {$group: {"_id": "$names", "firstNames": {$addToSet: "$names"}, "noOfUsers": {$sum: 1}} },
        {$unwind: "$firstNames"}
    ])
     */
    public Map<String, Long> groupAndCountUsersOfAccountByFirstName(Account ownerRef) {

        BasicDBObject ownerRefDBObject = new BasicDBObject("$ref", "account").append("$id", ownerRef.getId());

        Aggregation agg = newAggregation(
                match(where("ownerRef").is(ownerRefDBObject)),
                group().push("firstName").as("names"),
                unwind("names"),
                group("names").addToSet("names").as("firstName").count().as("noOfUsers"),
                unwind("firstName")
        );

        Map<String, Long> firstNamesMap = new HashMap<>();

        AggregationResults<FirstNameGroup> results =
                mongoTemplate.aggregate(agg, "user", FirstNameGroup.class);

        results.getMappedResults().stream().forEach(firstNameGroup ->
            firstNamesMap.put(firstNameGroup.getFirstName(), firstNameGroup.getNoOfUsers())
        );

        return firstNamesMap;
    }

    private class FirstNameGroup {

        private String firstName;
        private long noOfUsers;

        public FirstNameGroup(String firstName, long noOfUsers) {
            this.firstName = firstName;
            this.noOfUsers = noOfUsers;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public long getNoOfUsers() {
            return noOfUsers;
        }

        public void setNoOfUsers(long noOfUsers) {
            this.noOfUsers = noOfUsers;
        }
    }
}
