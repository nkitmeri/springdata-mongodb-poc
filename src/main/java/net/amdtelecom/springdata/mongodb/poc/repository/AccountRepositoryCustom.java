package net.amdtelecom.springdata.mongodb.poc.repository;

public interface AccountRepositoryCustom  {

    long updateAccountsField(String id, String field, Object value);
}
