package net.amdtelecom.springdata.mongodb.poc.repository;

import com.mongodb.WriteResult;
import net.amdtelecom.springdata.mongodb.poc.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Date;

import static org.springframework.data.mongodb.core.query.Criteria.*;

public class AccountRepositoryImpl implements AccountRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public long updateAccountsField(String id, String field, Object value) {

        Query query = new Query(
                where("_id")
                        .is(id)
                .and(field)
                        .exists(true)
        );

        Update update = new Update().set(field, value).set("updatedAt", new Date(System.currentTimeMillis()));

        WriteResult result = mongoTemplate.updateFirst(query, update, Account.class);

        return result.getN();
    }
}
