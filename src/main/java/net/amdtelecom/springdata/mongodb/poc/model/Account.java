package net.amdtelecom.springdata.mongodb.poc.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Set;

@Document(collection = "account")
@TypeAlias("Account")
@CompoundIndexes({
    @CompoundIndex(name = "company_info", def = "{'industry': 1, 'type': 1}")
})
public class Account {

    @Id
    private String id;

    @Indexed(name = "organizationName_1", unique = true)
    private String organizationName;

    private String industry;

    private OrganizationType type;

    private Long noOfUsers;

    @Transient
    private Set<User> users;

    @Indexed(name = "createdAt_1")
    private Date createdAt;

    private Date updatedAt;

    public Account() {
    }

    public Account(String id, String organizationName, String industry, OrganizationType type, Set<User> users, Date createdAt) {
        this.id = id;
        this.organizationName = organizationName;
        this.industry = industry;
        this.type = type;
        this.users = users;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public OrganizationType getType() {
        return type;
    }

    public void setType(OrganizationType type) {
        this.type = type;
    }

    public long getNoOfUsers() {
        return noOfUsers;
    }

    public void setNoOfUsers(long noOfUsers) {
        this.noOfUsers = noOfUsers;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public enum OrganizationType {
        INDIVIDUAL,
        COMPANY
    }
}
