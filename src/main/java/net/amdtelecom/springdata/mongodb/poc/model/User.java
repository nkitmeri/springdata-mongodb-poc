package net.amdtelecom.springdata.mongodb.poc.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user")
@TypeAlias("User")
public class User {

    @Id
    private String id;

    @Indexed(name = "userName_1")
    private String userName;

    @Indexed(name = "email_1")
    private String email;

    private String firstName;

    private String lastName;

    @DBRef(lazy = true)
    private Account ownerRef;

    public User() {
    }

    public User(String userName, String email, String firstName, String lastName, Account ownerRef) {
        this.userName = userName;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.ownerRef = ownerRef;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Account getOwnerRef() {
        return ownerRef;
    }

    public void setOwnerRef(Account ownerRef) {
        this.ownerRef = ownerRef;
    }
}
