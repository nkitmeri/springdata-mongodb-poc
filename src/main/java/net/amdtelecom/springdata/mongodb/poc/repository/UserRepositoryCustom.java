package net.amdtelecom.springdata.mongodb.poc.repository;

import net.amdtelecom.springdata.mongodb.poc.model.Account;

import java.util.Map;

public interface UserRepositoryCustom {

    Map<String, Long> groupAndCountUsersOfAccountByFirstName(Account ownerRef);
}
